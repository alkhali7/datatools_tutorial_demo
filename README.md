# DataTools_Tutorial_Demo
Set of jupyter notebooks created by students in CMSE495 (Michigan State University) that demonstrate some useful data science tools. 

This notebook includse an environment.yml file. Create and activate an enviornment using the following commands (Assumes anadonda or miniconda is installed):

conda env create --prefix ./envs --file environment.yml
conda activate ./envs

If you are new to git please review the following self guided curriculum:

- [Git Guide](https://msu-cmse-courses.github.io/cmse802-f20-student/0000--Jupyter-Getting-Started-Guide.html)

# Tutorials

## Python
### Basic Python
- [Basic Containers](Basic_Containers.ipynb)
- [Functions in Python](Functions_in_Python.ipynb)
- [Loops](Loops.ipynb)
### Packages
- [Numpy & Sympy](Numpy_Sympy.ipynb)
- [Pandas](Pandas.ipynb)
- [Seaborn](Seaborn Tutorial DTTD.ipynb)
- [Create a Python Package](Create_a_python_package.ipynb)

## Machine Learning
### AutoML
- [AutoML with SKLearn](Auto-SKLearn_AutoML)
- [Classification](Classification.ipynb)
- [GAMA AutoML](GAMA_AutoML_Tutorial.ipynb)
### Packages
- [PyTorch](PyTorch_tutorial.ipynb)
- [TPOT](tpot_tutorial.ipynb)

## Image Analysis
- [Automatic Cropping for Images](Auto_Cropping_Image_Tutorial)
- [Image Thresholding](image_thresholding_tutorial)

## GUI
- [Widgets](DTTD_Tutorial_Widgets-D2LAPITeam.ipynb)
- [GUI Tutorial](GUI_Tutorial.ipynb)

## Data Collection
- [CensusData Package](censusdata_package_tutorial)
- [Social Media Scraper](socail_media_scrapper)
- [Beautiful Soup](BeautifulSoup.ipynb)

## Math / Statistics
- [Big O Notation](BigO_C++.ipynb)
- [Central Limit Theorem](Central_Limit_Theorem.ipynb)
- [Eigenvalues](Eigenvalues.ipynb)
- [Gradients](Gradients.ipynb)
- [P Test](PTest.ipynb)
- [RREF](RREF.ipynb)
- [PCA](pcatutorial.ipynb)

## Text Analysis
- [FuzzyWuzzy](FuzzyWuzzy.ipynb)
- [Whitespace Indentation](Whitespace_Indentation.ipynb)

## C++
- [Pointers](Pointers.ipynb)
- [References](References.ipynb)
- [Scope C++](Scope_C++.ipynb)

## R
- [Tidyverse](Tidyverse_Tutorial.ipynb)

## Miscellaneous Tutorials
- [Audio Data Tutorial](AudioDataTutorial.ipynb)
- [Zotero](Zotero_Instructions.ipynb)

## Broken
- [Google Sheets](GoogleSheetsTutorial.ipynb)
- [Google Sheets 2](GoogleSheetsTutorial1.ipynb)
- [Getting Images from Video Stream](Video-Image-Data-Tutorial)




